/*
    Kopijuoti, pakeisti pavadinima pagal save
*/
var ctxLeft = document.getElementById("leftPieChart");
var ctxMiddle = document.getElementById("middlePieChart");
var ctxRight = document.getElementById("rightPieChart");
var ctxPolar = document.getElementById("leftPolarChart");
var ctxLine = document.getElementById("rightLineChart");

// Kairinis pyragas
var dataLeft = {
    labels: [
        "Miegas", "Darbas", "Kursai", "Laisvalaikis" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [9, 7, 3, 4], // Skilciu dydis
        backgroundColor: [
            "#3333ff", "#ff1a1a", "#ffff4d", "#33ff33" // Background spalvos
        ],
        hoverBackgroundColor: [ 
            "#33ff33", "#ffff4d", "#ff1a1a", "#3333ff" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartLeft = new Chart(ctxLeft, {
    type: 'pie',
    data: dataLeft,
});


// Vidurinis pyragas
var dataMiddle = {
    labels: [
        "Red", "Blue", "Yellow" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [15, 20, 30], // Skilciu dydis
        backgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Background spalvos
        ],
        hoverBackgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartMiddle = new Chart(ctxMiddle, {
    type: 'pie',
    data: dataMiddle
});


// Desininis pyragas
var dataRight = {
    labels: [
        "Red", "Blue", "Yello" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [15, 20, 30], // Skilciu dydis
        backgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Background spalvos
        ],
        hoverBackgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartRight = new Chart(ctxRight, {
    type: 'pie',
    data: dataRight
});

// Polar Chart
var dataPolar = {
    labels: [
        "Stuck between your sofa cushions", "Fridge or freeze", "Outdoors or in the car", "Nobody knows"
    ],

    datasets: [{
        data: [
            50, 4, 2, 44
        ],

        backgroundColor: [
            "#ccf2ff", "#b3ecff", "#99e6ff", "#80dfff"
        ]
    }]    
};

var myPolarChart = new Chart(ctxPolar, {
    type: 'polarArea',
    data: dataPolar
});

// Line Chart
var dataLine = {
    labels: ["0", "1", "2", "3", "4", "5", "6", "7"], // Skilciu pavadinimai
    datasets: [
        {
            label: "", // Duomenu pavadinimas
            fill: false, // Ar spalvinti apacia
            lineTension: 0.1, // Linijos apvalumas kuo maziau == labiau kampuota linija. Patariu tarp 0.01 ir 1
            backgroundColor: "#FF6384", // Background spalva
            borderColor: "#000000", // Linijos spalva
            borderCapStyle: "square", // tiksliai nezinau ka daro
            borderDash: [], // Galima linija padaryti dashed, 2 skaiciai pvz [15, 20]
            borderDashOffset: 0.0, // Tiksliai nezinau ka daro
            borderJoinStyle: 'bevel', // Tiksliai nezinau ka daro (bevel, round, miter)
            pointBorderColor: "#fff", // Tasku spalva
            pointBackgroundColor: "#fff", // Tasku sonu spalva
            pointBorderWidth: 1, // Tasku border dydis
            pointHoverRadius: 5, // Tasku dydis uzvedus pelyte
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "#82a83fa",
            pointHoverBorderWidth: 10,
            pointRadius: 1, // Tasku dydis
            pointHitRadius: 10,
            data: [-5, 0, 0.33, 0.66, 0.77, 0.80, 0.85, 0.92], // Duomenis
        }
    ]
};

var myLineChart = new Chart(ctxLine, {
    type: 'line',
    data: dataLine
});
