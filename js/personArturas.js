/*
    Kopijuoti, pakeisti pavadinima pagal save
*/
var ctxLeft = document.getElementById("leftPieChart");
var ctxMiddle = document.getElementById("middlePieChart");
var ctxRight = document.getElementById("rightPieChart");
var ctxPolar = document.getElementById("leftPolarChart");
var ctxLine = document.getElementById("rightLineChart");

// Kairinis pyragas
var dataLeft = {
    labels: [
        "Orange", "Purple", "Teal" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [15, 20, 30], // Skilciu dydis
        backgroundColor: [
            "Orange", "Purple", "teal" // Background spalvos
        ],
        hoverBackgroundColor: [ 
            "#FF6384", "#36A2EB", "#FFCE56" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartLeft = new Chart(ctxLeft, {
    type: 'pie',
    data: dataLeft,
});


// Vidurinis pyragas
var dataMiddle = {
    labels: [
        "Black", "Red", "Blue" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [15, 20, 30], // Skilciu dydis
        backgroundColor: [
            "Black", "Red", "Blue" // Background spalvos
        ],
        hoverBackgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartMiddle = new Chart(ctxMiddle, {
    type: 'pie',
    data: dataMiddle
});


// Desininis pyragas
var dataRight = {
    labels: [
        "Black", "Blue", "Grey" // Skilciu pavadinimai
    ],

    datasets:[{
        data: [15, 20, 30], // Skilciu dydis
        backgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Background spalvos
        ],
        hoverBackgroundColor: [
            "#FF6384", "#36A2EB", "#FFCE56" // Spalva uzejus su pelyte
        ]
    }]
};

var myPieChartRight = new Chart(ctxRight, {
    type: 'pie',
    data: dataRight
});

// Polar Chart
var dataPolar = {
    labels: [
        "Red", "Green", "Yellow", "Grey", "Blue"
    ],

    datasets: [{
        data: [
            10, 15, 20, 25, 30
        ],

        backgroundColor: [
            "#FF6384", "#4BC0C0", "#FFCE56", "#E7E9ED", "#36A2EB"
        ]
    }]    
};

var myPolarChart = new Chart(ctxPolar, {
    type: 'polarArea',
    data: dataPolar
});

// Line Chart
var dataLine = {
    labels: ["January", "February", "March", "April", "May", "June", "July"], // Skilciu pavadinimai
    datasets: [
        {
            label: "My First dataset", // Duomenu pavadinimas
            fill: false, // Ar spalvinti apacia
            lineTension: 0.1, // Linijos apvalumas kuo maziau == labiau kampuota linija. Patariu tarp 0.01 ir 1
            backgroundColor: "#FF6384", // Background spalva
            borderColor: "#000000", // Linijos spalva
            borderCapStyle: "square", // tiksliai nezinau ka daro
            borderDash: [], // Galima linija padaryti dashed, 2 skaiciai pvz [15, 20]
            borderDashOffset: 0.0, // Tiksliai nezinau ka daro
            borderJoinStyle: 'bevel', // Tiksliai nezinau ka daro (bevel, round, miter)
            pointBorderColor: "#fff", // Tasku spalva
            pointBackgroundColor: "#fff", // Tasku sonu spalva
            pointBorderWidth: 1, // Tasku border dydis
            pointHoverRadius: 5, // Tasku dydis uzvedus pelyte
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "#82a83fa",
            pointHoverBorderWidth: 10,
            pointRadius: 1, // Tasku dydis
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40], // Duomenis
        }
    ]
};

var myLineChart = new Chart(ctxLine, {
    type: 'line',
    data: dataLine
});
